## Profile
Organized, detail oriented, quality focused software engineer with 6+ years of experience. Highly proficient at Java and related development tools. Deep understanding of good software design principles, object-oriented design principles, design patterns, and API design.

*Contact*: recruitment[at]matthewjj.com

## Work Experience
### [Cerner Corporation](https://cerner.com), Kansas City, MO `Aug 2013-present`
- Associate Lead Software Engineer `Oct 2019-present`
- Senior Software Engineer `Oct 2017-Oct 2019`
- Software Engineer `Aug 2013-Oct 2017`
    - (Aug 2013-present) RevenueCycle Registration Team; desktop application written in Java built on the Eclipse framework using Eclipse SWT for UI
    - general duties include:
        - assisting in the planning of new projects
        - writing and reviewing technical designs for projects
        - writing new code/maintaining existing code
        - investigating and debugging client-reported issues
        - reviewing code from other teams working in Registration-owned projects
        - evaluating and improving our team’s development ecosystem from Maven to Git to Jenkins
    - other activities beyond Registration team:
        - mentoring new hires in the DevAcademy
        - assisting with recruiting events at college campuses
        - interviewing potential candidates for software engineering roles
- Software Engineer Intern `May 2012-Aug 2012`
    - RevenueCycle Registration Team

## Volunteer Experience
### [The Children's Mercy Hospital](http://childrensmercy.org), Kansas City, MO `Sep 2016-present`
- Child Life Volunteer
    - three hours per weekend
    - visit children's rooms, checking on patients and families
    - playing games, doing crafts, sitting with the children

### [TEALS](https://tealsk12.org) Teaching Assistant, Kansas City, MO `Jul 2017-May 2020`
- Blue Springs High School `Aug 2019-May 2020`
    - three mornings per week
    - assist with the CSA (Java-based) and CSP (computer science principles) courses
    - assist teacher with curriculum, lesson plans, class discussions, and grading
- Blue Springs High School `Aug 2018-May 2019`
    - three mornings per week
    - assist with the CSA (Java-based) and CSP (computer science principles) courses
    - assist teacher with curriculum, lesson plans, class discussions, and grading
- Blue Springs High School `Aug 2017-May 2018`
    - two mornings per week
    - assisted with the CSA (Java-based) and CSP (computer science principles) courses
    - assisted teacher with curriculum, lesson plans, class discussions, and grading

## Certifications
- Oracle Certified Associate, Java SE 7 Programmer `Jun 2017`

## Skills
Java, Eclipse SWT, Eclipse IDE, SVN, Git, Maven, Jenkins, mentoring and teaching

## Education
### Kansas State University, Manhattan, KS `Aug 2009-May 2013`
- B.S., Software Engineering `May 2013`
- Minor, Business `May 2013`

## References
- available upon request
